package ru.mynewtons.gameoflife.service;

import ru.mynewtons.gameoflife.domain.Game;

import java.util.Optional;

public interface GameService {
    Game save(Game game);

    Optional<Game> findById(String id);

    Game calculateNextStep(Game game);
}
