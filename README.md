Game of Life Backend
====================
Server side implementation of https://ru.wikipedia.org/wiki/%D0%98%D0%B3%D1%80%D0%B0_%C2%AB%D0%96%D0%B8%D0%B7%D0%BD%D1%8C%C2%BB

Usage
-----

* POST /api/game/ - save or update game entity
* GET /api/game/{id} - find game by id
* GET /api/game/{id}/next - calculate next game step, by game id

Request body example:
```
{
  "activeCells": [
    {
      "x": 0,
      "y": 0
    }
  ],
  "height": 0,
  "id": "string",
  "isActive": true,
  "width": 0
}
```
Run server
----------
Open terminal and execute following commands
0. Check mongodb run on your localhost machine on port 27017
1. git clone https://gitlab.com/svinokol/game-of-life.git
2. cd game-of-life
3. mvn clean install
4. java -jar ./target/game-of-life-0.0.1-SNAPSHOT.jar
5. Open browser window http://localhost:8080/swagger-ui.html

